.data
n: .word 8

.text
main:
    la t0, n
    lw a0, 0(t0)
    jal ra, factorial
    
    addi a1, a0, 0
    addi a0, x0, 1
    ecall # Print Result
    
    addi a0, x0, 10
    ecall # Exit

factorial:
    addi t1, x0, 1 # initialze t1 = 1 = result # t2 = n

fact:
    beq a0, x0, exit # check n == 0 return result
    mul t1, t1, a0 # multiplication lower bit
    addi a0, a0, -1
    jal x0, fact

exit:
    add a0, x0, t1
    jr ra
